# Praktikum Sistem Operasi 2003 Modul 1 (Kelompok A04)

## Daftar Isi
- [Anggota Kelompok A04](#anggota-kelompok)
- [Permasalahan 1](#permasalahan-1)
- [Permasalahan 2](#permasalahan-2)
- [Permasalahan 3](#permasalahan-3)
- [Permasalahan 4](#permasalahan-4)

## Anggota Kelompok
- Widian Sasi Disertasiani / 5025211024
- Kevin Nathanael Halim / 5025211140
- Andhika Lingga Mariano / 5025211161

## Permasalahan 1

### Latar Belakang
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file <i>.csv</i> yang berisi ranking universitas dunia untuk melakukan penataan strategi.

### Poin A
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

- Tujuan Permasalahan :
    <br>Mendapatkan 5 Universitas dengan ranking tertinggi di Jepang

- Konsep Penyelesaian :
    <br>
    Mencari Universitas di Jepang dengan melakukan filter terhadap setiap baris dengan kata kunci <b>'JP'</b> yang merupakan kode yang mewakili Jepang.
    <br>
    Karena pada file <i>.csv</i> yang diberikan, data sudah terurut berdasarkan ranking, maka pencarian 5 rank tertinggi dapat dilakukan hanya dengan pengambilan 5 baris teratas yang memenuhi syarat.

- Implementasi (Kode) :
    ```bash
    awk -F ',' '/JP/ {print $1 ". " $2}' "$file_path" | head -n 5
    ```

- Output Kode :
    ```
    23. The University of Tokyo
    36. Kyoto University
    55. Tokyo Institute of Technology (Tokyo Tech)
    68. Osaka University
    79. Tohoku University
    ```

- Penjelasan Kode :
    - `awk` : Melakukan pencarian terhadap kata kunci
        - `-F ','` : Memberi tahu bahwa delimiter yang memisahkan tiap kolom adalah tanda koma
        - `'/JP/ {print $1 ". " $2}'` : Kata kunci pencarian adalah JP dan aksi yang akan dilakukan adalah print terhadap nilai kolom 1 dan 2 yang merupakan ranking dan nama Universitas sesuai format.
        - `""$file_path"` : Path tempat tersimpannya file <i>.csv</i>
    <br><br>
    - `|` : Operator untuk memasukkan hasil dari perintah di kiri ke perintah di kanan
    <br><br>
    - `head` : Mengeluarkan n (default : 10) baris pertama ke standard output
        -  `-n 5` : Mengganti nilai n di atas dengan 5 yang artinya hanya 5 baris pertama yang akan dikeluarkan

### Poin B
Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang.

- Tujuan Permasalahan :
    <br>Mendapatkan 5 Universitas dengan Faculty Student Score(fsr score) yang terendah di Jepang

- Konsep Penyelesaian :
    <br>
    Mencari Universitas di Jepang dengan melakukan filter terhadap setiap baris dengan kata kunci <b>'JP'</b> yang merupakan kode yang mewakili Jepang.
    <br>
    Pada file <i>.csv</i> yang diberikan, data fsr score berada pada kolom 9, maka pencarian yang paling rendah dilakukan dengan melakukan sorting terhadap nilai dari kolom ke-9 secara ascending untuk baris yang memenuhi dan akhirnya akan diambil 5 baris teratas.

- Implementasi (Kode) :
    ```bash
    awk -F ',' '/JP/ {print $9 " : " $2}' "$file_path" | sort -g | head -n 5
    ```

- Output Kode :
    ```
    2.5 : Ritsumeikan Asia Pacific University
    2.6 : Shibaura Institute of Technology
    4.2 : Kwansei Gakuin University
    5.8 : Doshisha University
    5.8 : Meiji University
    ```

- Penjelasan Kode :
    - `awk` : Melakukan pencarian terhadap kata kunci
        - `-F ','` : Memberi tahu bahwa delimiter yang memisahkan tiap kolom adalah tanda koma
        - `'/JP/ {print $9 " : " $2}'` : Kata kunci pencarian adalah JP dan aksi yang akan dilakukan adalah print terhadap nilai kolom 9 dan 2 yang merupakan fsr score dan nama Universitas sesuai format.
        - `""$file_path"` : Path tempat tersimpannya file <i>.csv</i>
    <br><br>
    - `|` : Operator untuk memasukkan hasil dari perintah di kiri ke perintah di kanan
    <br><br>
    - `sort` : Melakukan pengurutan terhadap data berdasarkan suatu field yang telah ditentukan
        - `-g` : Melakukan sort dengan pembandingan menggunakan nilai numerik general (--general-numeric-sort)
    <br><br>
    - `head` : Mengeluarkan n (default : 10) baris pertama ke standard output
        -  `-n 5` : Mengganti nilai n di atas dengan 5 yang artinya hanya 5 baris pertama yang akan dikeluarkan

### Poin C
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

- Tujuan Permasalahan :
    <br>Mendapatkan 10 Universitas di Jepang dengan Employee Outcome Rank (ger rank) paling tinggi

- Konsep Penyelesaian :
    <br>
    Mencari Universitas di Jepang dengan melakukan filter terhadap setiap baris dengan kata kunci <b>'JP'</b> yang merupakan kode yang mewakili Jepang.
    <br>fsr score a pada kolom 20, maka pencarian yang paling tinggi dilakukan dengan melakukan sorting terhadap nilai dari kolom ke-20 secara ascending untuk baris yang memenuhi dan akhirnya akan diambil 10 baris teratas.

- Implementasi (Kode) :
    ```bash
    awk -F ',' '/JP/ {print $20 ". " $2}' "$file_path" | sort -g | head -n 10
    ```

- Output Kode :
    ```
    33. The University of Tokyo
    90. Keio University
    127. Waseda University
    169. Hitotsubashi University
    188. Tokyo University of Science
    201. Kyoto University
    367. Nagoya University
    377. Tokyo Institute of Technology (Tokyo Tech)
    379. International Christian University
    543. Kyushu University
    ```

- Penjelasan Kode :
    - `awk` : Melakukan pencarian terhadap kata kunci
        - `-F ','` : Memberi tahu bahwa delimiter yang memisahkan tiap kolom adalah tanda koma
        - `'/JP/ {print $20 ". " $2}'` : Kata kunci pencarian adalah JP dan aksi yang akan dilakukan adalah print terhadap nilai kolom 20 dan 2 yang merupakan ger ranking dan nama Universitas sesuai format.
        - `""$file_path"` : Path tempat tersimpannya file <i>.csv</i>
    <br><br>
    - `|` : Operator untuk memasukkan hasil dari perintah di kiri ke perintah di kanan
    <br><br>
    - `sort` : Melakukan pengurutan terhadap data berdasarkan suatu field yang telah ditentukan
        - `-g` : Melakukan sort dengan pembandingan menggunakan nilai numerik general (--general-numeric-sort)
    <br><br>
    - `head` : Mengeluarkan n (default : 10) baris pertama ke standard output

### Poin D
Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

- Tujuan Permasalahan :
    <br>Mendapatkan Universitas dengan kata kunci 'keren' (not case-sensitive)

- Konsep Penyelesaian :
    <br>
    Mencari baris yang mengandung kata <b>'keren'</b> (non-case-sensitive) dan kemudian melakukan print terhadap namanya

- Implementasi (Kode) :
    ```bash
    awk -F ',' 'tolower($0) ~/keren/ {print $2}' "$file_path"
    ```

- Output Kode :
    ```
    Institut Teknologi Sepuluh Nopember (ITS Surabaya Keren)
    ```

- Penjelasan Kode :
    - `awk` : Melakukan pencarian terhadap kata kunci
        - `-F ','` : Memberi tahu bahwa delimiter yang memisahkan tiap kolom adalah tanda koma
        - `'tolower($0) ~/keren/ {print $2}'` : Kata kunci pencarian adalah keren, dimana dilakukan tolower supaya pengecekkan terhadap baris dilakukan secara tidak case-sensitive. Kemudian, aksi yang akan dilakukan adalah print terhadap nilai kolom 2 yang merupakan nama Universitas.
        - `""$file_path"` : Path tempat tersimpannya file <i>.csv</i>

### Kendala yang sempat dihadapi :
- Di awal masih belum familiar dengan bagaimana file .csv bekerja sehingga perlu dilakukan beberapa kali percobaan untuk mendapatkan bahwa diperlukan delimiter berupa koma.
- Masih kurang familiar mengenai print dengan formatting di awal sehingga sempat mengalami kesalahan saat melakukan sorting karena pemilihan kolom dan delimiter yang salah.

## Permasalahan 2

### Latar Belakang
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

- Tujuan Permasalahan :
 <br>Membuat script yang dapat mengunduh gambar sebanyak X kali dimana X merupakan jam saat script dijalankan. File yang diunduh memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, merupakan urutan file yang diunduh (perjalanan_1, perjalanan_2, dst). Hasil unduhan gambar dimasukan ke folder  "Kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst), pengunduhan gambar dilakukan 10 jam sekali, lalu dizip sehari sekali. 

 - Konsep Penyelesaian :
 <br> Pertama harus melakukan cek direktori, yang bertujuan agar tidak ada file yang double, setelah pengecekan dilakukan, selanjutnya menentukan apabila jam menujukan pukul 00.00 maka dianggap 1, ketika tidak maka dilakukan looping yang akan dijelaskan di bawah. Setelah looping dilakukan, tahapan selanjutnya adalah melakukan zipping pada tiap folder, dengan mengecek apakah sudah ada file zip dengan nama yang sama atau tidak, jika tidak maka akan dilakukan looping. Seperti Berikut. 

## Solusi 

Membuat fungsi dlDir() yang akan digunakan untuk mengexecute unduhan gambar serta peletakan gamar ke dalam folder kumpulan_NOMOR.

Untuk melakukan pengecekan apakah folder `kumpulan` sudah ada pada direktori yang sama atau belum, dengan variabel index sebagai nilai index folder terakhir.

```bash
index=1

while [ -d "kumpulan_$index" ]
do 
    index=$((index+1))
done
mkdir "kumpulan_$index" 
```

Kemudian melakukan command untuk masuk ke dalam folder `kumpulan` serta membuat variable untuk menampung waktu.
```bash
HOUR=$(date +"%H")
cd "kumpulan_$index"
```
Lalu membuat looping sesuai dengan ketentuan soal, yaitu : Apabila waktu 00.00 maka hanya dilakukan pengunduhan sebanyak 1 kali.
``` bash
if [[ $HOUR == 0 ]]
then 
HOUR=1
fi
```
Looping selanjutnya dibuat dengan kondisi apabila waktu menunjukan selain pukul 00.00, maka pengunduhan gambar dilakukan sebanyak angka pada `JAM`

``` bash
for (( i=1; i<=$HOUR; i++ ))
do 
    wget -cO - https://dagodreampark.co.id/images/ke_2.jpg > perjalanan_$i.jpg
done
```

Tahap kedua adalah membuat fungsi zipDir() yang digunakan untuk mengexecute pengubahan file batch ```kumpulan``` menjadi `Devil_NOMOR.zip`

Dilakukan pengecekan apakah file `Devil_NOMOR.zip` sudah ada atau belum, jika belum ada maka file zip akan dibuat dengan index terakhir. dengan `ayam` sebagai index.

``` bash
ayam=1
while [ -d kumpulan_$ayam ]
do 
    if [ ! -f devil_$ayam.zip ]
    then
        zip -r  "devil_$ayam.zip" kumpulan_$ayam
    fi

    ayam=$((ayam+1))
done 
```

Untuk mengexecute salah satu dari 2 fungsi tersebut perlu dilakukan code seeprti ini. Agar user dapat memilih action yang akan dilakukan melalui input parameter. 

``` bash
inp=$1

if [ $inp = "d" ]
then
   dlDir
elif [ $inp = "z" ]
then
   zipDir
else
   echo "char input invalid (d/z only)"
fi
```

Cara pengaksesan salah satu fungsi tersebut adalah dengan dilakukan command seperti ini:
```bash
#mengakses fungsi 1
bash kobeni_liburan.sh d

#mengakses fungsi 2
bash kobeni_liburan.sh z
```
## CronJob

Berikut merupakan command mengatur jadwal pengunduhan serta jadwal zipping pada script di atas.

```
# cronjob untuk melakukan download setiap 10 jam
# 0 */10 * * *  /home/kobeni_liburan.sh d
```

````
# cronjob untuk melakukan zip setiap 1 hari
# 0 */24 * * *  /home/kobeni_liburan.sh z
````

### Output Download
<img src="https://cdn.discordapp.com/attachments/995337235211763722/1083685841081225226/image.png">
<img src="https://cdn.discordapp.com/attachments/995337235211763722/1083686263762202644/image.png">

### Output Zip
<img src="https://cdn.discordapp.com/attachments/995337235211763722/1083686463343968367/image.png">
<img src="https://cdn.discordapp.com/attachments/995337235211763722/1083686681762336778/image.png">

### Kendala yang sempat dihadapi
- Di awal, sempat terjadi kesalahan penulisan logika looping sehingga proses zipping tidak berjalan sesuai dengan keinginan
- Terjadi kebingungan terkait cara mengaplikasikan cronjob setiap 10 jam setelah script dijalankan

## Permasalahan 3

### Latar Belakang
Peter Griffin hendak membuat suatu sistem register pada script <b>louis.sh</b> dari setiap user yang berhasil didaftarkan di dalam file <i>/users/users.txt</i>. Peter Griffin juga membuat sistem login yang dibuat di script <b>retep.sh</b>.

- Tujuan Permasalahan :
<br>Membuat sistem registrasi dan sistem login di mana username dan password pengguna yang berhasil mendaftar akan dicatat di dalam file <i>/users/users.txt</i>. Setiap percobaan registrasi dan login juga akan dicatat pada file <i>log.txt</i> dengan format YY/MM/DD hh:mm:ss MESSAGE, di mana MESSAGE tersebut akan bergantung pada keberhasilan user dalam melakukan registrasi atau login.

- Konsep Penyelesaian :
<br>Sebelum melakukan registrasi atau login, dilakukan pengecekan terlebih dahulu terhadap file <i>/users/users.txt</i> apakah sudah ada atau belum. Jika belum, maka file tersebut akan dibuat. 
<br><br>Pada sistem registrasi, username dan password yang dimasukkan akan dicek.
    - Pengecekan username pada file <i>/users/users.txt</i> dilakukan dengan menggunakan command <b>grep</b>. Apabila username yang sama ditemukan dalam file <i>/users/users.txt</i>, maka message "Register Gagal" akan dikirim ke file <i>log.txt</i>. Sementara apabila username yang sama tidak ditemukan dalam file <i>/users/users.txt</i>, maka username tersebut dapat digunakan dan message "Register Berhasil" akan dikirim ke file <i>log.txt</i>.

    - Pengecekan password dilakukan menggunakan beberapa statement If Else sesuai dengan ketentuan berikut:
        - Minimal 8 karakter
        - Memiliki minimal 1 huruf kapital dan 1 huruf kecil
        - Alphanumeric
        - Tidak boleh sama dengan username 
        - Tidak boleh menggunakan kata chicken atau ernie
   
        Apabila username dan password yang dimasukkan telah sesuai, maka user telah berhasil melakukan registrasi. Setelah itu, username dan password dari user tersebut akan dicatat di dalam file <i>/users/users.txt</i>.

    Pada sistem login, username dan password akan dicek. 
    - Pengecekan username pada file <i>/users/users.txt</i> dilakukan dengan menggunakan command <b>grep</b>. Apabila username yang sama ditemukan dalam file <i>/users/users.txt</i>, maka username tersebut telah terdaftar dan akan diminta untuk memasukkan password.
    - Pengecekan password pada file <i>/users/users.txt</i> juga dilakukan dengan menggunakan command <b>grep</b>. Apabila password tidak sesuai dengan username, maka message "Login Gagal" akan dikirim ke file <i>log.txt</i>. Sementara apabila password sesuai dengan username, maka user telah berhasil login dan message "Login Berhasil" akan dikirim ke file <i>log.txt</i>.

## Solusi

Pertama-tama, program akan mengecek terlebih dahulu apakah sudah terdapat file <i>users/users.txt</i> atau belum. Jika belum, maka file users.txt akan dibuat di dalam directory users.
```bash
if ! test -f users/users.txt;
then 
    mkdir users
    cd users
    touch users.txt
    cd ..
fi
```

### Sistem Registrasi (louis.sh)
Program masuk ke dalam while loop dan meminta input username dari user dengan command <b>read</b> berikut 
```bash
read -p "Enter Username: " username
```
Kemudian program mengecek username yang dimasukkan dengan menggunakan command <b>grep</b>. Karena username yang telah didaftarkan akan tercatat pada file <i>users/users.txt</i>, maka jika username tersebut ditemukan dalam file <i>users/users.txt</i>, program akan mengirim message 

```bash
REGISTER: ERROR User USERNAME already exists 
```

ke dalam file <i>log.txt</i> dan meminta user untuk memasukkan ulang username yang berbeda. Sementara jika username yang dimasukkan belum pernah didaftarkan, maka program akan keluar dari while loop.
```bash
if grep -Fwq "$username" users/users.txt;
then echo -e "Username $username already exist!\n"
    
echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt

else break
fi
```
Setelah input username diterima, program kembali masuk ke dalam while loop untuk meminta input password dari user menggunakan command <b>read</b>.
```bash
read -p "Enter Password: " password
```
Kemudian program akan melakukan pengecekan agar password yang dimasukkan sesuai dengan ketentuan pada soal.
1. Mengecek apakah password terdiri dari minimal 8 karakter
```bash
if [ ${#password} -lt 8 ]
then echo -e "Invalid Password! Password must be at least 8 characters\n"
```

2. Mengecek apakah password terdiri dari minimal 1 huruf kapital dan 1 huruf kecil
```bash
elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* ]]
then echo -e "Invalid Password! Password must contain at least 1 upper case letter and 1 lower case letter\n"
```

3. Mengecek apakah password terdiri dari gabungan antara huruf dan angka (*alphanumeric*)
```bash
elif [[ ! "$password" =~ [0-9] ]]
then echo -e "Invalid Password! Password must be alphanumeric\n"
```
4. Mengecek apakah password yang dimasukkan sama dengan username
```bash
elif [ "$password" == "$username" ]
then echo -e "Invalid Password! Password must not be the same as username\n"
```
5. Mengecek apakah password menggunakan kata '*chicken*' atau '*ernie*'
```bash
elif [[ "$password" == *"chicken"*  || "$password" == *"ernie"* ]]
then echo -e "Invalid Password! Password must not contain the word 'chicken' or 'ernie'\n"
```

Apabila password yang dimasukkan tidak memenuhi salah satu syarat di atas, maka user akan diminta untuk memasukkan ulang passwordnya. Sementara jika password yang dimasukkan telah sesuai, maka program akan keluar dari while loop.

Setelah berhasil melakukan registrasi, username beserta password tersebut akan dimasukkan ke dalam file <i>users/users.txt</i> dengan format 
```bash
username, password
```

dan message 

```bash
REGISTER: INFO User USERNAME registered successfully 
```

juga akan dikirim ke file <i>log.txt</i>. 

```bash
echo "Registration Successful!"
echo "$username, $password" >> users/users.txt
echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt 
```
- Output Kode
<img src="https://cdn.discordapp.com/attachments/1024601733583880205/1083645860920971304/image.png">
<img src="https://cdn.discordapp.com/attachments/1024601733583880205/1083646481187213322/image.png">
Berikut merupakan isi file <i>users/users.txt</i> dan <i>log.txt</i> setelah user melakukan registrasi
<img src="https://cdn.discordapp.com/attachments/1024601733583880205/1083648608731136060/image.png">
<img src="https://cdn.discordapp.com/attachments/1024601733583880205/1083648164269137972/image.png">

### Sistem Login (retep.sh)
Program masuk ke dalam while loop dan meminta input username dari user dengan perintah <b>read</b> berikut 
```bash
read -p "Enter Username: " username
```
Kemudian program mengecek username yang dimasukkan dengan menggunakan command <b>grep</b>. Apabila username tersebut telah terdaftar dalam file <i>users/users.txt</i>, maka program akan langsung keluar dari while loop.

Sementara apabila username yang dimasukkan tidak ditemukan di dalam file <i>users/users.txt</i>, hal ini berarti username tersebut belum terdaftar dan program akan meminta user untuk memasukkan ulang username yang lain. 
```bash
if grep -Fwq "$username," users/users.txt;
then break

else echo -e "Username not found! Try again\n"
fi
```
Setelah input username diterima, program kembali masuk ke dalam while loop untuk meminta input password dari user menggunakan command <b>read</b>.
```sh
read -p "Enter Password: " password
```

Password tersebut kemudian dicek apakah sesuai dengan username yang telah didaftarkan sebelumnya. Jika sesuai, maka message

```bash
LOGIN: INFO User USERNAME logged in
```

akan dikirim ke file <i>log.txt</i> dan program akan keluar dari while loop.

```sh
if grep -Fw "$username," users/users.txt | grep -Fwq " $password";
then
  echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
  echo "Login Successful!"
  break
```

Sementara jika password yang dimasukkan tidak sesuai, maka message

```bash
LOGIN: ERROR Failed login attempt on user USERNAME
```

akan dikirim ke file <i>log.txt</i> dan user akan diminta untuk memasukkan ulang passwordnya.

```sh
else
  echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
  echo -e "Wrong Password! Try Again\n"
fi
```

- Output Kode
<img src="https://cdn.discordapp.com/attachments/1024601733583880205/1083648956707381358/image.png">
Berikut merupakan isi file <i>log.txt</i> setelah user melakukan login
<img src="https://cdn.discordapp.com/attachments/1024601733583880205/1083649026659983400/image.png">

### Kendala yang sempat dihadapi
- Pada tahap registrasi, hasil yang diinginkan saat ingin mengecek username pada file <i>/users/users.txt</i> sempat tidak sesuai. Di mana ketika username yang dimasukkan memiliki kesamaan dengan password dari user lain, registrasi tidak dapat dilakukan.  
- Pada tahap login, pengecekan password pada file <i>/users/users.txt</i> juga sempat tidak sesuai. Di mana ketika password tidak sesuai dengan username yang dimasukkan namun terdapat password yang sama pada akun lain, user tetap dapat melakukan login.

## Permasalahan 4

### Latar Belakang
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya.

### Poin A & B
1. Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).<br>
2. Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
    - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    - Setelah huruf z akan kembali ke huruf a


- Tujuan Permasalahan :
    <br>Melakukan backup terhadap file syslog dengan format nama sesuai dengan yang tertera menggunakan waktu terkini, kemudian dilakukan enkripsi terhadap log system tersebut dengan sistem cipher berdasarkan jam terkini.

- Konsep Penyelesaian :
    <br>
    Mengambil isi dari log system pada /var/log/syslog kemudian membuat nama file dengan waktu terkini.
    <br>
    Kemudian mengambil jam terkini dan membuat string untuk cipher berdasarkan nilai jam tersebut.
    <br>
    Terakhir, isi dari log system dienkripsi dengan cipher yang sudah diperoleh dan segera dimasukkan ke dalam file baru dengan nama yang sudah dibuat sebelumnya ke dalam folder backup log system.

- Implementasi (Kode) :
    ```bash
    file_path="/home/kev/Kuliah/Sisop/files1/log_system_dir"
    cd $file_path

    h=$(date +%H)
    log=$(cat /var/log/syslog)
    log_system_file="$(date +"%H:%M %d:%m:%Y").txt"

    i=0 #Baris ini tidak terpakai, tetapi lupa dihapus
    sAlphabet=$(echo {a..z} | tr -d [:space:])
    bAlphabet=$(echo {A..Z} | tr -d [:space:])
    sAlphabet=${sAlphabet:$h:26-$h}${sAlphabet:0:$h}
    bAlphabet=${bAlphabet:$h:26-$h}${bAlphabet:0:$h}

    encrypt=$(echo "$log" | tr 'a-z' $sAlphabet | tr 'A-Z' $bAlphabet)
    echo "$encrypt" > "$log_system_file"
    ```

- Output Kode :
    <img src="https://cdn.discordapp.com/attachments/995337235211763722/1083557287702057080/image.png">

- Penjelasan Kode :
    - `file_path` : Variabel penampung absolute path dari directory tempat menyimpan hasil backup log system
        - `cd $file_path` : Untuk berpindah ke directory yang sudah dispesifikasikan pada variabel <b>file_path</b>    
    <br><br>
    - `h=$(date +%H)` : Variabel penampung jam terkini yang diambil menggunakan formating dari <b>date</b> yang akan menampilkan tanggal dan waktu terkini
    - `log=$(cat /var/log/syslog)` : Variabel penampung isi dari log system yang merupakan hasil <b>cat</b> dari <i>/var/log/syslog</i>
    - `log_system_file="$(date +"%H:%M %d:%m:%Y").txt"` : Variabel penampung nama file yang diperolah dari hasil formatting <b>date</b> dengan mengambil jam, menit, tanggal, bulan, dan tahun terkini. Ditambah dengan ekstensi <b>.txt</b> yang menandakan file tersebut adalah text file.
    <br><br>
    - `sAlphabet=$(echo {a..z} | tr -d [:space:])` : Variabel yang menampung hasil dari <b>echo</b> terhadap <b>{a..z}</b> yang akan mengeluarkan string `"a b c d.. z"`. Kemudian string tersebut dipassing ke <b>tr -d [:space:]</b> yang berfungsi untuk menghilangan seluruh spasi dari string tersebut sehingga menjadi `"abcd..z"`.
    - `bAlphabet=$(echo {A..Z} | tr -d [:space:])` : Sama persis dengan yang di atas, hanya saja untuk abjad besar (A-Z).
    <br><br>
    - `sAlphabet=${sAlphabet:$h:26-$h}${sAlphabet:0:$h}` : Mengganti nilai variabel 'sAlphabet' dengan penggabungan substring dari index ke-h sampai 26-h-1 dengan substring dari index ke-0 sampai index ke-h-1. Maka akan diperoleh string cipher yang digunakan untuk melakukan enkripsi.
    - `bAlphabet=${bAlphabet:$h:26-$h}${bAlphabet:0:$h}` : Sama persis dengan yang di atas, hanya saja untuk abjad besar (A-Z).
    <br><br>
    - `encrypt=$(echo "$log" | tr 'a-z' $sAlphabet | tr 'A-Z' $bAlphabet)` : Variabel penampung isi dari log yang kemudian dienkripsi dengan cipher yang telah dibuat sebelumnya menggunakan <b>tr</b> dengan string 'a-z' dan 'A-Z' terhadap string pada variabel `sAlphabet` dan `bAlphabet`
        - `echo "$encrypt" > "$log_system_file"` : Mengeluarkan isi dari variabel `encrypt` ke dalam file dengan nama yang sudah dispesifikan tadi pada variabel `log_system_file` dengan <b>echo</b>


### Poin C
Buat juga script untuk dekripsinya.

- Tujuan Permasalahan :
    <br>Membuat script untuk dekripsi file backup yang telah dienkripsi, hasil dari Poin A & B

- Konsep Penyelesaian :
    <br>
    Mendapatkan nama file yang akan dibuka melalui parameter dan mengambil isi dari file hasil enkripsi yang telah didapatkan.
    <br>
    Mengambil jam dari nama file dan dilakukan dekripsi berdasarkan jam yang telah diperoleh.
    <br>
    Terakhir, dioutputkan hasilnya yang merupakan text asli dari log_system.

- Implementasi (Kode) :
    ```bash
    file_path="/home/kev/Kuliah/Sisop/files1/log_system_dir"

    log_name="$1"
    h=${log_name:0:2}

    i=0 #Baris ini tidak terpakai, tetapi lupa dihapus
    sAlphabet=$(echo {a..z} | tr -d [:space:])
    bAlphabet=$(echo {A..Z} | tr -d [:space:])
    sAlphabet=${sAlphabet:$h:26-$h}${sAlphabet:0:$h}
    bAlphabet=${bAlphabet:$h:26-$h}${bAlphabet:0:$h}

    encrypted_system_file="$file_path/$log_name"

    decrypt=$(cat "$encrypted_system_file" | tr $sAlphabet 'a-z' | tr $bAlphabet 'A-Z')
    echo $decrypt
    ```

- Output Kode :
    <img src="https://cdn.discordapp.com/attachments/995337235211763722/1083558672770277506/image.png">

- Penjelasan Kode :
    - `file_path` : Variabel penampung absolute path dari directory tempat menyimpan hasil backup log system  
    <br><br>
    - `log_name="$1"` : Variabel penampung isi dari parameter yang merupakan nama dari file yang ingin didekripsi isinya.
        - `h=${log_name:0:2}` : Variabel penampung jam yang didapatkan dari substring 2 index pertama dari nama file log enkripsi.
    - `log_system_file="$(date +"%H:%M %d:%m:%Y").txt"` : Variabel penampung nama file yang diperolah dari hasil formatting <b>date</b> dengan mengambil jam, menit, tanggal, bulan, dan tahun terkini. Ditambah dengan ekstensi <b>.txt</b> yang menandakan file tersebut adalah text file.
    <br><br>
    `encrypted_system_file="$file_path/$log_name"` : Variabel penampung path dari file backup log yang terenkripsi.
    <br><br>
    - `decrypt=$(cat "$encrypted_system_file" | tr $sAlphabet 'a-z' | tr $bAlphabet 'A-Z')` : Mengambil isi dari file yang pathnya dispesifikasikan pada variabel `encrypted_system_file` dengan <b>cat</b>. Kemudian dilakukan <b>tr</b> dengan string pada variabel `sAlphabet` dan `bAlphabet` terhadap string 'a-z' dan 'A-Z' untuk mengembalikan huruf-huruf yang telah terenkripsi menjadi normal.
        - `echo $decrypt` : Mengeluarkan isi dari variabel `decrypt` ke terminal dengan <b>echo</b>

### Poin D
Backup file syslog setiap 2 jam untuk dikumpulkan 😀.

- Tujuan Permasalahan :
    <br>Melakukan backup terhadap file syslog pada <i>/var/log/syslog</i> setiap 2 jam sekali

- Konsep Penyelesaian :
    <br>
    Menjalankan cron job yang akan digunakan untuk melakukan run terhadap script `log_encrypt.sh` setiap 2 jam sekali.

- Implementasi (Kode) :
    ```bash
    0 */2 * * *  /home/kev/Kuliah/Sisop/Praktikum1/soal4/log_encrypt.sh
    ```

- Output Kode :
    ```
    Terbuatnya file.txt di log_system_dir setiap 2 jam sekali
    ```

- Penjelasan Kode :
    - `0 */2 * * *` : 0 berarti cronjob akan menjalankan perintah setiap menit ke-0 dan */2 berarti cronjob akan menjalankan perintah setiap 2 jam sekali. Sementara itu, 3 bintang sisanya mewakili tanggal, bulan, dan hari dalam minggu yang dapat diabaikan karena tidak digunakan.
    - `/home/kev/Kuliah/Sisop/Praktikum1/soal4/log_encrypt.sh` : File path yang merupakan tempat tersimpannya script `log_encrypt.sh` yang diminta untuk dijalankan setiap 2 jam sekali.

### Kendala yang sempat dihadapi
- Sempat agak kesulitan untuk melakukan enkripsi, awalnya menggunakan metode brute force dengan iterasi per huruf sehingga memakan waktu yang sangat lama. Barulah setelah menemukan tr, script bisa jauh lebih optimal.
- Cronjob juga sempat tidak aktif, namun setelah dicoba-coba dan diusahakan akhirnya bisa berjalan dengan baik.