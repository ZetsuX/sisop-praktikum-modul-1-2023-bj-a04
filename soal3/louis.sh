#!/bin/bash

echo "REGISTRATION SYSTEM"

# Cek apakah file users/users.txt sudah ada
# Jika belum, maka file users/users.txt akan dibuat
if ! test -f users/users.txt;
then 
    mkdir users
    cd users
    touch users.txt
    cd ..
fi

while [ 1 ]
do
    #Input username
    read -p "Enter Username: " username

    # Cek apakah username sudah ada dalam file users/users.txt
    if grep -Fwq "$username," users/users.txt;
    then echo -e "Username $username already exist!\n"
    
    # Message "registrasi gagal" dimasukkan ke dalam file log.txt
    echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt

    else  break
    fi
done

while [ 1 ]
do
    # Input password
    read -p "Enter Password: " password

    # Cek apakah password memiliki minimal 8 karakter
    if [ ${#password} -lt 8 ]
    then echo -e "Invalid Password! Password must be at least 8 characters\n"

    # Cek apakah password memiliki minimal 1 huruf kapital dan 1 huruf kecil
    elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* ]]
    then echo -e "Invalid Password! Password must contain at least 1 upper case letter and 1 lower case letter\n"

    # Cek apakah password memiliki minimal 1 angka (alphanumeric)
    elif [[ ! "$password" =~ [0-9] ]]
    then echo -e "Invalid Password! Password must be alphanumeric\n"

    # Cek apakah password sama dengan username
    elif [ "$password" == "$username" ]
    then echo -e "Invalid Password! Password must not be the same as username\n"
 
    # Cek apakah password menggunakan kata 'chicken' atau 'ernie'
    elif [[ "$password" == *"chicken"*  || "$password" == *"ernie"* ]]
    then echo -e "Invalid Password! Password must not contain the word 'chicken' or 'ernie'\n"

    else break
    fi
done

# Registrasi berhasil
echo "Registration Successful!"

# Masukkan username dan password ke dalam file users/users.txt
echo "$username, $password" >> users/users.txt

# Message "registrasi berhasil" dimasukkan ke dalam file log.txt
echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt


