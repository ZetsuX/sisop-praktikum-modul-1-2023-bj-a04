#!/bin/bash

echo "LOGIN SYSTEM"

# Cek apakah file users/users.txt sudah ada
# Jika belum, maka file users/users.txt akan dibuat
if ! test -f users/users.txt;
then 
    mkdir users
    cd users
    touch users.txt
    cd ..
fi

while [ 1 ]
do
    #Input username
    read -p "Enter Username: " username

    # Cek apakah username sudah ada dalam file users/users.txt
    if grep -Fwq "$username," users/users.txt;
    then break

    else echo -e "Username not found! Try again\n"
    fi
done 

while [ 1 ]
do
    #Input password
    read -p "Enter Password: " password

    # Cek apakah password sesuai dengan username pada file users/users.txt
    if grep -Fw "$username," users/users.txt | grep -Fwq " $password";
    then
        # Message "login berhasil" dimasukkan ke dalam file log.txt
        echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
        echo "Login Successful!"
        break
    else
        # Message "login gagal" dimasukkan ke dalam file log.txt
        echo "$(date '+%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
        echo -e "Wrong Password! Try Again\n"
    fi
done
