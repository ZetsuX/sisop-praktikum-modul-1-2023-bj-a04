#!/bin/bash

# Mendapatkan Absolute Path dari Directory Log System Backup
file_path="/home/kev/Kuliah/Sisop/files1/log_system_dir"

# Mendapatkan jam dari nama File yang dimasukkan lewat parameter
log_name="$1"
h=${log_name:0:2}

# Membuat string untuk tr berdasarkan jam dengan shifting terhadap string alfabet normal
i=0
sAlphabet=$(echo {a..z} | tr -d [:space:])
bAlphabet=$(echo {A..Z} | tr -d [:space:])
sAlphabet=${sAlphabet:$h:26-$h}${sAlphabet:0:$h}
bAlphabet=${bAlphabet:$h:26-$h}${bAlphabet:0:$h}

# Melakukan pengambilan encrypted file dari path dan nama yang diberikan
encrypted_system_file="$file_path/$log_name"

# Melakukan dekripsi file encrypted dengan 2x tr yang kemudian dioutputkan ke terminal
decrypt=$(cat "$encrypted_system_file" | tr $sAlphabet 'a-z' | tr $bAlphabet 'A-Z')
echo $decrypt