#!/bin/bash

# Pergi ke Absolute Path untuk Directory Log System Backup
file_path="/home/kev/Kuliah/Sisop/files1/log_system_dir"
cd $file_path

# Mendapatkan jam terkini
h=$(date +%H)

# Mendapatkan isi dari syslog
log=$(cat /var/log/syslog)

# Membuat nama file dari date
log_system_file="$(date +"%H:%M %d:%m:%Y").txt"

# Membuat string untuk tr berdasarkan jam dengan shifting terhadap string alfabet normal
i=0
sAlphabet=$(echo {a..z} | tr -d [:space:])
bAlphabet=$(echo {A..Z} | tr -d [:space:])
sAlphabet=${sAlphabet:$h:26-$h}${sAlphabet:0:$h}
bAlphabet=${bAlphabet:$h:26-$h}${bAlphabet:0:$h}

# Melakukan enkripsi terhadap string dengan 2x tr yang kemudian ditulis ke file yang sudah dispesifikasikan
encrypt=$(echo "$log" | tr 'a-z' $sAlphabet | tr 'A-Z' $bAlphabet)
echo "$encrypt" > "$log_system_file"

# Isi crontab -e untuk menjalankan script setiap 2 jam :
# 0 */2 * * *  /home/kev/Kuliah/Sisop/Praktikum1/soal4/log_encrypt.sh