#!/bin/bash

dlDir(){
   index=1

   #membuat dir
   while [ -d "kumpulan_$index" ]
   do 
      index=$((index+1))
   done
   mkdir "kumpulan_$index"

   HOUR=$(date +"%H")

   #masuk ke dalam file kumpulan_

   cd "kumpulan_$index"

   #jika jam 00.00

   if [[ $HOUR == 0 ]]
   then 
   HOUR=1
   fi

   #jika selain jam 00.00

   for (( i=1; i<=$HOUR; i++ ))
   do 
      wget -cO - https://dagodreampark.co.id/images/ke_2.jpg > perjalanan_$i.jpg
   done
}

#untuk zip
zipDir(){
   ayam=1
   while [ -d kumpulan_$ayam ]
   do 
      if [ ! -f devil_$ayam.zip ]
      then
         zip -r  "devil_$ayam.zip" kumpulan_$ayam
      fi

      ayam=$((ayam+1))
   done
}

inp=$1

if [ $inp = "d" ]
then
   dlDir
elif [ $inp = "z" ]
then
   zipDir
else
   echo "char input invalid (d/z only)"
fi

# cronjob untuk download setiap 10 jam
# 0 */10 * * *  /home/kobeni_liburan.sh d

# cronjob untuk melakukan zip setiap 1 hari
# 0 */24 * * *  /home/kobeni_liburan.sh z