#!/bin/bash

#Absolute Path untuk File CSV
file_path="/home/kev/Kuliah/Sisop/files1/2023 QS World University Rankings.csv"

# a) Menampilkan 5 Universitas dengan ranking tertinggi di Jepang
awk -F ',' '/JP/ {print $1 ". " $2}' "$file_path" | head -n 5
echo ""

# b) Mencari Faculty Student Score(fsr score) yang paling rendah dari semua universitas di Jepang
awk -F ',' '/JP/ {print $9 " : " $2}' "$file_path" | sort -g | head -n 5
echo ""

# c) Mencari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi
awk -F ',' '/JP/ {print $20 ". " $2}' "$file_path" | sort -g | head
echo ""

# d) Mencari Universitas dengan kata kunci keren (non-case sensitive)
awk -F ',' 'tolower($0) ~/keren/ {print $2}' "$file_path"